=== Link Google Calendar ===
Contributors: dxladner
Tags: calendar, Google Calendar, Google Calendar Embed Link, Embedded Google Calendar
Donate link: https://hyperdrivedesigns.com/
Requires at least: 4.0
Tested up to: 4.5
Stable tag: trunk
License: GPLv2 or later
License URI:  http://www.gnu.org/licenses/gpl-2.0.html

A plugin that allows administrator to set Google Calendar embedded link in admin back-end and use shortcode
to place on a page, post or sidebar.

== Description ==
A plugin that allows administrator to set Google Calendar embedded link in admin back-end and use a shortcode
to place on a page, post or sidebar.

NEW FEATURES:
We have listened to our users and have added the ability to have multiple Google Calendars. You can have up to 5 different
Google Calendars now. As of right now, your calendar will still show up on the front end with the current saved settings,
but for the future, you will need to make some changes to the settings. So how does your current calendar still work.
Pretty simple. The original calendar used the [lgc] shortcode which is saved in the database along with your Google Calendar
info which will still display your current calendar. So everything is still working.

If you want to finish the update and be up to date with the plugin or you are one of the users that would like to have multiple Google
Calendars on your website then you will have to redo the settings. No worries it is not that hard. Please read the complete
documentation located <a href="https://hyperdrivedesigns.com/link-google-calendar-update-documentation/">here</a>.

== Installation ==
This section describes how to install the plugin and get it working.

1. Download and install the plugin from WordPress dashboard. You can also upload the entire “link-google-calendar” folder to the `/wp-content/plugins/` directory

2. Activate the plugin through the ‘Plugins’ menu in WordPress

3. You should see a Link Google Calendar Options section in the Admin Menu. Click Number of Calendars link to go to the Calendar Settings Page.

4. Set the number of calendars you would like to use on your website.

5. Now click on the Link Google Calendar Options link to go to the Calendar Settings Page.

4. Here you would place your Embedded Google Calendar Link in the textarea and then click the save button.

5. You would get your Calendar from your Google Account. For information on how to create your Google Calendar and how to get
your Embedded Google Calendar Link, go to our website <a href="https://hyperdrivedesigns.com/free-plugins/google-calendar-link-plugin/">Hyperdrive Designs</a>.

6. Use the shortcode that are generated and displayed above each calendar setting option to place the calendar in your page, post or a text widget.

== Frequently Asked Questions ==
 Has this plugin been tested

Yes. This plugin has been tested from WordPress version 3.5 - 4.5.

Can I change the size of my Google Calendar

Yes. You can change the size of our calendar on the settings page. Look at your Embedded Link that you got from Google. There is a width and height settings you can edit.

 What about support?

Create a support ticket at Hyperdrive Designs website <a href="https://hyperdrivedesigns.com/js-support-ticket-controlpanel/">SUPPORT</a> and I will take care of any issue. If you have any issues or have any questions, please contact me and give me a chance to help you with your issue.

== Screenshots ==
1. new settings page
2. new calendar options page
3. calendar page view
4. calendar post
5. calendar sidebar

== Changelog ==
2.0
Major updates: Link Google Calendar can how have up to 5 calendars. New Calendar Options page and New Calendar Settings page.

1.2
Change in shortcode output code to correct development warnings.

1.1
Change in shortcode output code.

1.0
Initial Commit

== Upgrade Notice ==
2.0
Major updates: Link Google Calendar can how have up to 5 calendars. New Calendar Options page and New Calendar Settings page.

1.2
This version creates a very small change to the shortocde output which fixes warning issues when users have their site in development mode with WP_DEBUG enabled.If you upgraded to version 1.1 then you might want to go ahead and install this version so you will not see any warnings if for some reason you have WP_DEBUG enabled.

1.1
This version allows the user to place the calendar shortcode anywhere on the page and have the calendar appear exactly where you place the shortcode.
The first version still works fine. It places the calendar at the top of the page. New version places the calendar exactly where you place the shortcode on the page.

1.0
Initial Commit
